package application.service;

import application.entity.Emp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @title: EmpService
 * @author: 流沐颖
 * @date:2022/6/17 19:01
 * @description:
 * @version: study
 */
public interface EmpService extends IService<Emp> {

    /**
     * 获取所有的emp数据
     *
     * @return
     */
    List<Emp> getAllEmp();

    /**
     * 根据empno查询
     *
     * @param empno
     * @return
     */
    Emp getEmpById(Integer empno);

    /**
     * 根据empno删除emp
     *
     * @param empno
     * @return
     */
    boolean removeEmpByempno(Integer empno);

    /**
     * 添加操作
     * @param emp
     * @return
     */
    boolean saveEmp(Emp emp);
    /**
     * 添加操作
     * @param emp
     * @return
     */
    boolean saveEmp2(Emp emp);

    /**
     * 更新操作
     * @param emp
     * @return
     */
    boolean updateEmpById(Emp emp);
}
