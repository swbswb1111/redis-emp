package application.service.impl;

import application.entity.Emp;
import application.mapper.EmpMapper;
import application.service.EmpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @title: EmpServiceImpl
 * @author: 流沐颖
 * @date:2022/6/17 19:01
 * @description:
 * @version: study
 */
@Service
public class EmpServiceImpl extends ServiceImpl<EmpMapper, Emp> implements EmpService {


    @Autowired
    private RedisTemplate redisTemplate;

    private String REDISCODE = "emp:list";

    private long timeout=5;


    /**
     * 获取到所有的emp
     *
     * @return
     */
    @Override
    public List<Emp> getAllEmp() {
        ListOperations listOperations = redisTemplate.opsForList();
        // 从redis中读取数据
        List<Emp> empList = (List<Emp>) listOperations.range(REDISCODE, 0, -1);
        // 如果从redis中获取不到则去数据库中进行查询
        if (empList.size() == 0) {
            // 将查询的结果放到集合中
            empList = super.list();
            for (Emp emp : empList) {
                // 将查询的结果存到redis
                listOperations.leftPush(REDISCODE, emp);
            }
        }
        return empList;
    }

    /**
     * 根据id查询
     *
     * @param empno
     * @return
     */
    @Override
    public Emp getEmpById(Integer empno) {
        // 先从redis中获取到相关数值
        ListOperations listOperations = redisTemplate.opsForList();

        listOperations.index(REDISCODE, 0);

        Emp emp = super.getById(empno);

        return emp;
    }

    /**
     * 根据empno删除emp
     *
     * @param empno
     * @return
     */
    @Override
    public boolean removeEmpByempno(Integer empno) {
        ListOperations listOperations = redisTemplate.opsForList();
        List<Emp> allEmp = getAllEmp();
        for (Emp emp : allEmp) {
            if (emp.getEmpno().equals(empno)) {
                // 先删除数据库中的数据
                boolean flag = super.removeById(empno);
                if (flag) {
                    // 删除redis的中的数据
                    listOperations.remove(REDISCODE, 0, emp);
                }
                return flag;
            }
        }
        return false;
    }

    /**
     * 添加操作
     *
     * @param emp
     * @return
     */
    @Override
    public boolean saveEmp(Emp emp) {
        ListOperations listOperations = redisTemplate.opsForList();
        if (!ObjectUtils.isEmpty(emp)) {
            // 将数据保存到数据库中
            boolean save = super.save(emp);
            if (save) {
                // 将数据保存到redis中
                listOperations.rightPush(REDISCODE, emp);
            }
            return save;
        }
        return false;
    }

    /**
     * 添加操作
     *
     * @param emp
     * @return
     */
    @Override
    public boolean saveEmp2(Emp emp) {
        // 将数据添加到数据库中
        super.save(emp);
        // 直接删掉缓存中的数据
        redisTemplate.delete(REDISCODE);
        return true;
    }

    /**
     * 更新操作
     *
     * @param emp
     * @return
     */
    @Override
    public boolean updateEmpById(Emp emp) {
        ListOperations listOperations = redisTemplate.opsForList();
        // 得到所有的数据 与我们输入进去的empno进行对比
        List<Emp> allEmp = getAllEmp();
        //拿到要进行修改的index
        int index = 0;
        if (!ObjectUtils.isEmpty(emp)) {
            // 先更新mysql
            boolean update = super.updateById(emp);
            if (update) {

                // 因为要得到相应的index 所以不可使用foreach
                for (int i = 0; i < allEmp.size(); i++) {
                    if (allEmp.get(i).getEmpno().equals(emp.getEmpno())) {
                        index = i;
                        break;
                    }
                }
                //更新redis
                listOperations.set(REDISCODE, index, emp);
            }
            return update;
        }
        return false;
    }
}
