package application.mapper;

import application.entity.Emp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @title: EmpMapper
 * @author: 流沐颖
 * @date:2022/6/17 18:54
 * @description:
 * @version: study
 */
@Mapper
public interface EmpMapper extends BaseMapper<Emp> {

}
