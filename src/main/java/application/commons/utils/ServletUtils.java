package application.commons.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @title: ServletUtils
 * @author: 流沐颖
 * @date:2022/6/28 14:51
 * @description: 对servlet的常用方法进行封装
 * @version: study
 */
public class ServletUtils {
    /**
     * 获得request对象
     * @return
     */
    public static HttpServletRequest getRequest(){
        // 拿到request的父类
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        return request;
    }

    /**
     * 获得session
     * @return
     */
    public static HttpSession getSessions(){
       return   getRequest().getSession();
    }

    /**
     * 获取请求url
     */
    public static String getUrl(){
        return getRequest().getRequestURI().toString();
    }

    /**
     * 获得远程地址
     */
    public static String getCurrentRemote(){
        return getRequest().getRemoteAddr();
    }
}
