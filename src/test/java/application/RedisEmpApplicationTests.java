package application;

import application.entity.Emp;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import sun.reflect.generics.tree.VoidDescriptor;

import javax.print.attribute.standard.MediaSize;
import java.lang.reflect.Type;
import java.util.List;

@SpringBootTest
class RedisEmpApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
    }


    /**
     * 测试查询
     */
    @Test
    public List<Emp> getEmp() {

        ListOperations listOperations = redisTemplate.opsForList();


        // 将得到的字符串强转为emp对象
        List<Emp> empList = (List<Emp>) listOperations.range("emp:list", 0, -1);

        System.out.println(empList.size());

        for (Emp emp : empList) {
            System.out.println(emp);
        }
        return empList;
    }

    /**
     * 测试添加
     */
    @Test
    public void saveEmp() {
        ListOperations listOperations = redisTemplate.opsForList();
        // 准备一个对象
        Emp emp = new Emp();
        emp.setEname("aaa");
        emp.setDeptno(10);

        Long re = listOperations.rightPush("emp:list", emp);
        if (re > 0) {
            System.out.println("插入成功！");
        } else {
            System.out.println("插入失败");
        }

    }

    /**
     * 测试更新
     */
    @Test
    public void updateEmp() {
        ListOperations listOperations = redisTemplate.opsForList();
        // 准备一个对象
        Emp emp = new Emp();
        emp.setEmpno(7903);
        emp.setEname("aaa");
        emp.setDeptno(10);
        emp.setDname("奥特曼");

        List<Emp> emp1 = getEmp();
        int index=0;
        for (int i = 0; i < emp1.size(); i++) {
            if (emp1.get(i).getEmpno().equals(emp.getEmpno())) {
                System.out.println("===============");
                System.out.println(emp1.get(i).getEmpno());
                index=i;
                System.out.println("===============");
                break;
            }
        }
        listOperations.set("emp:list",index,emp);


    }

    @Test
    public void empInfo(){
        ListOperations listOperations = redisTemplate.opsForList();

        System.out.println(listOperations.index("emp:list", 0));

    }

}
